package com.jledesma.composeproject.util

object Constants {

    const val ON_BOARDING_PAGE_COUNT = 3
    const val LAST_ON_BOARDING_PAGE = 2

    const val PREFERENCES_NAME = "compose_project_preferences"
    const val PREFERENCES_KEY = "on_boarding_complete"

}