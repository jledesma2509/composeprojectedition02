package com.jledesma.composeproject.domain.repository

import com.jledesma.composeproject.domain.model.User
import com.jledesma.composeproject.core.Result
import kotlinx.coroutines.flow.Flow

interface UserRepository {

    suspend fun authenticateUser(email:String,password:String) : Flow<Result<User>>


}