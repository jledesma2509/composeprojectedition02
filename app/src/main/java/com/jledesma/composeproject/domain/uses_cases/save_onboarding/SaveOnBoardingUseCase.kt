package com.jledesma.composeproject.domain.uses_cases.save_onboarding

import com.jledesma.composeproject.domain.repository.DataStoreOperations

class SaveOnBoardingUseCase(private val repository: DataStoreOperations) {

    suspend operator fun invoke(completed: Boolean) {
        repository.saveOnBoardingState(completed = completed)
    }

}