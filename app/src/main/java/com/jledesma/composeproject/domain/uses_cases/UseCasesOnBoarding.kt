package com.jledesma.composeproject.domain.uses_cases

import com.jledesma.composeproject.domain.uses_cases.read_onboarding.ReadOnBoardingUseCase
import com.jledesma.composeproject.domain.uses_cases.save_onboarding.SaveOnBoardingUseCase

data class UseCasesOnBoarding(
    val saveOnBoardingUseCase: SaveOnBoardingUseCase,
    val readOnBoardingUseCase: ReadOnBoardingUseCase
)
