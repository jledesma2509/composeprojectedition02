package com.jledesma.composeproject.domain.uses_cases.read_onboarding

import com.jledesma.composeproject.domain.repository.DataStoreOperations
import kotlinx.coroutines.flow.Flow


class ReadOnBoardingUseCase(private val repository : DataStoreOperations) {

    operator fun invoke() : Flow<Boolean> {
        return repository.readOnBoardingState()
    }

}