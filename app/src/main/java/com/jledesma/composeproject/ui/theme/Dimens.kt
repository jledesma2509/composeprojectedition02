package com.jledesma.composeproject.ui.theme

import androidx.compose.ui.unit.dp

val EXTRA_LARGE_PADDING = 40.dp
val SMALL_PADDING = 10.dp

val PAGING_INDICADOR_WIDTH = 12.dp
val PAGING_INDICADOR_SPACING = 8.dp