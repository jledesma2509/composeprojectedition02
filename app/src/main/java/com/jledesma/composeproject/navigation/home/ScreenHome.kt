package com.jledesma.composeproject.navigation.home

import com.jledesma.composeproject.navigation.Screen

sealed class ScreenHome(val route:String){

    object Home : ScreenHome("home_screen")
    object Chat : ScreenHome("chat_screen")
    object Settings : ScreenHome("settings_screen")


}
