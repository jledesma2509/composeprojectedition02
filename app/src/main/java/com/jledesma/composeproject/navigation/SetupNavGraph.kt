package com.jledesma.composeproject.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.jledesma.composeproject.presentation.home.HomeScreen
import com.jledesma.composeproject.presentation.login.LoginScreen
import com.jledesma.composeproject.presentation.splash.SplashScreen
import com.jledesma.composeproject.presentation.welcome.WelcomeScreen

@Composable
fun SetupNavGraph(navController: NavHostController) {

    NavHost(
        navController = navController,
        startDestination = Screen.Login.route){

        composable(route = Screen.Splash.route){
            SplashScreen(navHostController = navController)
        }
        composable(route = Screen.Welcome.route){
            WelcomeScreen(navHostController = navController)
        }
        composable(route = Screen.Login.route){
            LoginScreen(navHostController = navController)
        }
        composable(route = Screen.Home.route){
            HomeScreen()
        }

    }

}