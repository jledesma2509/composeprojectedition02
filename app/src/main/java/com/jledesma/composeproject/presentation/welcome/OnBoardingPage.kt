package com.jledesma.composeproject.presentation.welcome

import androidx.annotation.DrawableRes
import com.jledesma.composeproject.R

sealed class OnBoardingPage(
    @DrawableRes
    val image:Int,
    val title:String,
    val description:String
){
    object First : OnBoardingPage(
        image = R.drawable.greetings,
        title = "Greetings",
        description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
    )
    object Second : OnBoardingPage(
        image = R.drawable.explore,
        title = "Explore",
        description = "Lorem Ipsum has been the industry's standard dumm industry's standard dummy text "
    )
    object Third : OnBoardingPage(
        image = R.drawable.power,
        title = "Power",
        description = "not only five centuries, but also the leap into electronic typesetting"
    )
}
