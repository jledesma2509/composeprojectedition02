package com.jledesma.composeproject.presentation.splash

import android.content.res.Configuration
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import com.jledesma.composeproject.R
import com.jledesma.composeproject.ui.theme.Purple500
import com.jledesma.composeproject.ui.theme.Purple700
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationVector1D
import androidx.compose.animation.core.tween
import androidx.compose.runtime.*
import androidx.compose.ui.draw.rotate
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.jledesma.composeproject.navigation.Screen

@Composable
fun SplashScreen(
    navHostController: NavHostController ,
    splashViewModel: SplashViewModel = hiltViewModel()) {

    val onBoardingComplete by splashViewModel.onBoardingComplete.collectAsState()

    val degrees = remember {
        Animatable(0f)
    }

    LaunchedEffect(key1 = true){

        degrees.animateTo(
            targetValue = 360f,
            animationSpec = tween(
                durationMillis = 1000,
                delayMillis = 200
            )
        )
        navHostController.popBackStack()
        if(onBoardingComplete){
            navHostController.navigate(Screen.Login.route)
        }else{
            navHostController.navigate(Screen.Welcome.route)
        }

    }
    
    Splash(degrees = degrees.value)
}

@Composable
fun Splash(degrees: Float) {

    if (isSystemInDarkTheme()) { //Dark
        SplashContent(
            modifier = Modifier
                .background(Color.Black)
                .fillMaxSize(),degrees)
    } else { //Light
       SplashContent(
           modifier = Modifier
               .background(Brush.verticalGradient(listOf(Purple700, Purple500)))
               .fillMaxSize(),degrees)
    }


}

@Composable
fun SplashContent(modifier: Modifier, degrees: Float) {
    Box(
        modifier = modifier,
        contentAlignment = Alignment.Center
    ) {
        Image(
            modifier = Modifier.rotate(degrees = degrees),
            painter = painterResource(id = R.drawable.ic_logo),
            contentDescription = "Logo"
        )
    }
}

@Preview(showSystemUi = true)
@Composable
fun SplashLightPreview() {
    Splash(degrees = 0f)
}

@Preview(showSystemUi = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
fun SplashDarkPreview() {
    Splash(degrees = 0f)
}