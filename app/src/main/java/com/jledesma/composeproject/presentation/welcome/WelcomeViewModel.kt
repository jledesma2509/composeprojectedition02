package com.jledesma.composeproject.presentation.welcome

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jledesma.composeproject.domain.uses_cases.UseCasesOnBoarding
import com.jledesma.composeproject.domain.uses_cases.save_onboarding.SaveOnBoardingUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WelcomeViewModel
@Inject constructor(
    private val usesCasesOnBoarding: UseCasesOnBoarding) : ViewModel() {

    fun saveOnBoardingState(completed:Boolean){
        viewModelScope.launch(Dispatchers.IO) {
            usesCasesOnBoarding.saveOnBoardingUseCase(completed = completed)
        }
    }

}