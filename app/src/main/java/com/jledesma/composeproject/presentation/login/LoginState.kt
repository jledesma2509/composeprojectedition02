package com.jledesma.composeproject.presentation.login

import com.jledesma.composeproject.domain.model.User

data class LoginState(
    val isLoading: Boolean = false,
    val error:String? = null,
    val user:User? = null
)
